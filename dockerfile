from python:3

RUN pip install flask

COPY hello.py .

EXPOSE 5000

ENTRYPOINT FLASK_APP=hello.py flask run --host=0.0.0.0